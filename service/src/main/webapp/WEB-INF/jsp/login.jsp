<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
  <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
      <!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
      <html>

      <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/login.css">
        <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/css/consentimentBanner.css">
        <script src="/js/cookies.js" type="text/javascript"></script>
        <script src="/js/consentimentBanner.js" type="text/javascript"></script>

        <title>Login</title>
      </head>

      <body>

        <div class="menu-btn">
          <i class="fas fa-bars fa-2x"></i>
        </div>

        <div class="container">
          <!-- Header -->
          <header id="home-header">
            <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

            <nav class="main-nav">
              <ul class="main-menu">
                <li><a href="/">Home</a></li>
                <li><a href="quiSom">Qui Som</a></li>
                <li><a href="buscarReserva">Reservar tractament</a></li>
              </ul>
              <ul class="right-menu">
                <li><a href="login">Login</a></li>
                <li><a href="tipusUsuari">Registre</a></li>
              </ul>
            </nav>
          </header>

          <!--Main-->
          <main>

            <div class="container-main">

              <div class="welcome">
                <img src="/img/welcome.jpg" alt="welcome">
              </div>

            <form:form method="post" id="login" modelAttribute="login" action="/login">
              <table align="center">
                <tr>
                  <td>
                    <form:label path="nomUsuari">Nom d'usuari: </form:label>
                  </td>
                  <td>
                    <form:input path="nomUsuari" name="nomUsuari" id="nomUsuari" />
                  </td>
                </tr>
                <tr>
                  <td>
                    <form:label path="contrasenya">Contrasenya:</form:label>
                  </td>
                  <td>
                    <form:password path="contrasenya" name="contrasenya" id="contrasenya" />
                  </td>
                  <td>${message}</td>
                </tr>
                <tr>
                  <td></td>
                  <td align="left">
                    <form:button id="login" name="login" class="btn">Entrar</form:button>
                  </td>
                </tr>
                <tr></tr>
                <tr>
                  <td></td>
                </tr>
              </table>
            </form:form>

          </div>

        </main>       

        <!--Footer-->
        <footer id="main-footer" class="footer">
          <div class="footer-inner">
            <div><img src="/img/Dark-logo.png" class="logo"></div>
            <ul>
              <li><a href="#">Contacte</a></li>
              <li><a href="#">Politica de la Web</a></li>
              <li><a href="#">&copy; Beauty Service 2022</a></li>
            </ul>
          </div>

        </footer>

        </div>

                            <!--seccio consentiment banner-->
                            <div class="cookie-banner">
                              <div class="cookie-close accept-cookie"></div>
                              <div class="container">
                                <p>Utilitzem cookies pr&#242;pies per a un &#250;s t&#233;cnic i per a un bon funcionament dels nostres serveis.
                                  <br>Si accepteu o continueu navegant, considerem que accepteu les condicions.<br>
                                  <a href="https://www.boe.es/buscar/pdf/2018/BOE-A-2018-16673-consolidado.pdf"> Per m&#233;s infomaci&#243; sobre
                                    la protecci&#243; de dades</a>
                                </p>
                                <button type="button" class="btn acceptar-cookie">Aceptar</button>
                              </div>
                            </div> 

      </body>

      </html>
