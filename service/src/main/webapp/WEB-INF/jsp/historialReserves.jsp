<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
            <!DOCTYPE html>
            <html>

            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta http-equiv="X-UA-Compatible" content="ie=edge">
                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
                <link rel="stylesheet" href="/css/style.css">
                <link rel="stylesheet" href="/css/benvingut.css">
                <script src="/js/home.js" type="text/javascript"></script>
                <script src="/js/cookies.js" type="text/javascript"></script>
                <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
                <script src="https://kit.fontawesome.com/7c1b1fcd24.js" crossorigin="anonymous"></script>
                <title>Les meves reserves</title>
            </head>

            <body>

                <!-- Responsive menu -->

                <div class="menu-btn">
                    <i class="fas fa-bars fa-2x"></i>
                </div>

                <div class="container">
                    <!-- Header -->
                    <header id="home-header">
                        <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

                        <nav class="main-nav">
                            <ul class="main-menu">
                                <li><a href="/">Home</a></li>
                                <li><a href="quiSom">Qui Som</a></li>
                                <li><a href="buscarReserva">Buscar Tractament</a></li>
                            </ul>
                            <ul class="right-menu">
                                <li><a href="#">Hola ${nomUsuari}</a></li>
                                <li><i class="fa-solid fa-user"></i></li>
                                <li><a href="logout">Tancar sessio</a></li>
                            </ul>
                        </nav>
                    </header>

                    <!-- Main area-->
                    <main>

                        <section class="main-section">

                            <!-- My Profile Menu -->

                            <section id="lateral-menu">
                                <div>
                                    <nav class="main-menu">
                                        <ul class="menuLateral">
                                            <li><a href="buscarReserva">Cercar un servei per reservar</a></li>
                                            <li><a href="lesMevesReserves">Les meves reserves</a></li>
                                            <li><a href="lesMevesDades">Les meves dades</a> </li>
                                            <li><a href="historialReserves?idUsuari=${idUsuari}">El meu historial de reserves</a></li>
                                            <li><a href="baixa">Donar-te de baixa</a></li>
                                        </ul> 
                                    </nav>                                                                  
                                </div>
                            </section>

                            <!-- Responsive Profile Menu-->
                            <div class="profile-menu">
                                <button onclick="myFunction()" class="dropbtn">Les Meves Opcions</button>
                                <div id="myDropdown" class="dropdown-content">
                                    <a href="buscarReserva">Cercar un servei per reservar</a>
                                    <a href="lesMevesReserves">Les meves reserves</a>
                                    <a href="lesMevesDades">Les meves dades</a>
                                    <a href="historialReserves?idUsuari=${idUsuari}">El meu historial de reserves</a>
                                    <a href="baixa">Donar-te de baixa</a>
                                </div>                           
                            </div>

                            <!-- Reserves -->
                            <section class="reserves-card">
                                <c:choose>
                                    <c:when test="${fn:length(reserves) == '0'}">
                                        <div>
                                            <p>No tens cap reserva a l'historial</p>
                                            <a href="buscarReserva">Buscar un servei per reservar</a>
                                        </div>
                                        
                                    </c:when>
                                    <c:otherwise>
                                        <section id="reserves-cards" class="reserves-cards">
                                            <div class="reserves">
                                            <h3>Historial de reserves</h3>
                                        </div>
                                        <div>   
                                            <div class="card-content">
                                                <c:forEach items="${reserves}" var="reserves" varStatus="loop">
                                                    <tr>
                                                        <h3 class="card-title">Reserva ja gaudida</h3>
                                                        <img src="/img/skincare.jpg" alt="skincare">
                                                        <p>${reserves.text}</p>
                                                        <h4><a class="btn"
                                                                href="detallReservaHistorial?idSalo=${reserves.idSalo}&idReserva=${reserves.idReserva}">Detall
                                                                de la reserva</a></h4>
                                                    </tr>
                                                </c:forEach>
                                            </div>
                                        </div>
                                        </section>
                                    </c:otherwise>                                 
                                </c:choose>
                            </section>      
               
                    </main>

                    <!--Footer-->
                    <footer id="main-footer" class="footer">
                        <div class="footer-inner">
                            <div><img src="/img/Dark-logo.png" class="logo"></div>
                            <ul>
                                <li><a href="#">Contacte</a></li>
                                <li><a href="#">Politica de la Web</a></li>
                                <li><a href="#">&copy; Beauty Service 2022</a></li>
                            </ul>
                        </div>

                    </footer>

                </div>

            </body>

            </html>