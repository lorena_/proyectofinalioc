<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/lesMevesDades.css">
        <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/css/consentimentBanner.css">
        <script src="/js/cookies.js" type="text/javascript"></script>
        <script src="/js/consentimentBanner.js" type="text/javascript"></script>        
        <title>Llistar tipus tipusServei</title>
    </head>

    <body>

        <!-- Responsive menu -->

        <div class="menu-btn">
            <i class="fas fa-bars fa-2x"></i>
        </div>

        <div class="container">

            <!-- Header -->
            <header id="home-header">
                <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

                <nav class="main-nav">
                    <ul class="main-menu">
                        <li><a href="/">Home</a></li>
                        <li><a href="quiSom">Qui Som</a></li>
                        <li><a href="buscarReserva">Reservar tractament</a></li>
                    </ul>
                    <ul class="right-menu">
                        <li><a href="#">Hola ${nomUsuari}</a></li>
                        <li><i class="fa-solid fa-user"></i></li>
                        <li><a href="logout">Tancar sessio</a></li>
                    </ul>
                </nav>
            </header>

            <!-- Main area-->
            <main>

                <section class="main-section">

                    <!-- My Profile Menu -->

                    <section id="lateral-menu">

                    </section>

                    <section id="dades">
                        <div class="dades">
                            <h3>Dades tipus de servei:</h3>
                            <table>
                                <thead>
                                    <tr>
                                        <th>Descripcio&#769; del tipus servei</th>
                                        <th>Nom tipus servei</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${llistarTipusServeis}" var="llistarTipusServei" varStatus="loop">
                                        <tr>
                                            <td>${llistarTipusServei.nomTipusServei}</td>
                                            <td>${llistarTipusServei.descripcioTipus}</td><br>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>

                        </div>


                    </section>

                </section>               

                <!--Footer-->
                <footer id="main-footer" class="footer">
                    <div class="footer-inner">
                        <div><img src="/img/Dark-logo.png" class="logo"></div>
                        <ul>
                            <li><a href="#">Contacte</a></li>
                            <li><a href="#">Politica de la Web</a></li>
                            <li><a href="#">&copy; Beauty Service 2022</a></li>
                        </ul>
                    </div>

                </footer>
        </div>

    </body>

    </html>