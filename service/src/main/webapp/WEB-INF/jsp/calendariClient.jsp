
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

  <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="ISO-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Calendari</title>
        <link href="css/main.css" rel="stylesheet" type="text/css">
        <link href="icons/style.css" rel="stylesheet" type="text/css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/css/consentimentBanner.css">
        <script src="/js/cookies.js" type="text/javascript"></script>
        <script src="/js/consentimentBanner.js" type="text/javascript"></script>       
    </head>
    <body>
        <div class="main" style="display: flex;">
            <div style="margin-right: 10px;">
                <div id="nav"></div>
            </div>
            <div style="flex-grow: 1">
                <h2><h3>Selecciona un dia i una hora</h3>
                <div id="dp"></div>
            </div>
        </div>
        <!-- DayPilot Pro library-->
        <script src="js/daypilot/daypilot-all.min.js"></script>

        <!-- basic calendar config -->
       
        <div id="dp"></div>
        <div id="nav"></div>
        <h2><a id="link">Clica per solicitar reserva</a></h2>
        <p id="avis">(Primer selecciona un dia i despres una hora)</p>
        <script>
            const dp = new DayPilot.Calendar("dp");
             dp.locale = "es-es";
             dp.onTimeRangeSelected = async args => {
                const form = [
                     {
                        name: '${tipusServei}',
                     }
                 ];
                 const data = {
                     name: '${tipusServei}'
                 };
                 const modal = await DayPilot.Modal.form(form, data);

                 dp.clearSelection();
                 if(modal.canceled) {
                     return;
                 }
                 if(!modal.canceled) {
                    
                 }
                 const params = {
                    start: args.start,
                    end: args.end,
                    text: modal.result.name,
                    resource: args.resource
                };
                const {data: result} = await DayPilot.Http.post("/api/events/create", params);
                dp.events.add(result);
                dp.message("Reserva guardada");
                let idEvent = result.id;
                let idSalo = '${idSalo}';
                let linkGuardar = document.getElementById("link");
                let avis = document.getElementById("avis");
                linkGuardar.setAttribute("href", "resumReserva?idSalo="+idSalo+"&idEvent=" + idEvent);
                avis.style.visibility = "hidden";
                };
             dp.init();
        
             const nav = new DayPilot.Navigator("nav");
             nav.showMonths = 2;
             nav.skipMonths = 2;
             nav.selectMode = "week";
             nav.onTimeRangeSelected = (args) => {
                dp.startDate = args.day;
                dp.update();
            };
            nav.init();
        </script>


    </body>
</html>