<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
    <!DOCTYPE html>
    <html>

    <head>

      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/lesMevesDades.css">
        <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
        <link rel="stylesheet" href="/css/consentimentBanner.css">
        <script src="/js/cookies.js" type="text/javascript"></script>
        <script src="/js/consentimentBanner.js" type="text/javascript"></script>        
        <title>Reserva un servei</title>
    </head>

    <body>

        <!-- Responsive menu -->

        <div class="menu-btn">
            <i class="fas fa-bars fa-2x"></i>
        </div>

        <div class="container">

            <!-- Header -->
            <header id="home-header">
                <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

                <nav class="main-nav">
                    <ul class="main-menu">
                      <li><a href="/">Home</a></li>
                      <li><a href="quiSom">Qui Som</a></li>
                      <li><a href="buscarReserva">Reservar tractament</a></li>
                    </ul>
                    <ul class="right-menu">
                      <li><a href="#">Hola ${nomUsuari}</a></li>
                      <li><i class="fa-solid fa-user"></i></li>
                      <li><a href="logout">Tancar sessio</a></li>
                    </ul>
                </nav>
            </header>

            <!-- Main area-->
            <main>

                <section class="main-section">

                    <!-- My Profile Menu -->

                    <section id="lateral-menu">

                    </section>
                    <section id="dades">
                        <div class="dades">
                            <h3>Tria un salo&#769; i un servei</h3>

                            <form:form  id="tipusServeis" modelAttribute="tipusServeis"
                            action="registreClientTipusServeiProcess" method="POST">
                                <table>
                                  <tr>
                                    <td><b>Nom salo&#769;:</b></td>
                                
                                    <td>
  
                                      <form:select path="nomTipusServei">
                                        <c:forEach items="${llistarSalons}" var="llistarSalon" varStatus="loop">
                                          <form:option value="${llistarSalon.nomSalo}"></form:option>
                                      </c:forEach>    
                                      </form:select>
                                    </td>
                                  </tr>
                                  <tr>
                                    <td><b>Tipus de servie</b></td>
                                    <td>

                                      <form:select path="nomTipusServei">
                                                <c:forEach items="${llistarTipusServeis}" var="llistarTipusServei" varStatus="loop">
                                                <form:option value="${llistarTipusServei.nomTipusServei}"></form:option>
                                            </c:forEach>         
                                      </form:select>

                                    </td>
                                  </tr>
                                  <tr>
                                    <td><input type="submit" value="Submit"></td>
                                  </tr>
                                </table>
                              </form:form>

                        </div>


                    </section>

                </section>

                    <!--seccio consentiment banner-->
                    <div class="cookie-banner">
                      <div class="cookie-close accept-cookie"></div>
                      <div class="container">
                        <p>Utilitzem cookies pr&#242;pies per a un &#250;s t&#233;cnic i per a un bon funcionament dels nostres serveis.
                          <br>Si accepteu o continueu navegant, considerem que accepteu les condicions.<br>
                          <a href="https://www.boe.es/buscar/pdf/2018/BOE-A-2018-16673-consolidado.pdf"> Per m&#233;s infomaci&#243; sobre
                            la protecci&#243; de dades</a>
                        </p>
                        <button type="button" class="btn acceptar-cookie">Aceptar</button>
                      </div>
                    </div>                

                <!--Footer-->
                <footer id="main-footer" class="footer">
                    <div class="footer-inner">
                        <div><img src="/img/Dark-logo.png" class="logo"></div>
                        <ul>
                            <li><a href="#">Contacte</a></li>
                            <li><a href="#">Politica de la Web</a></li>
                            <li><a href="#">&copy; Beauty Service 2022</a></li>
                        </ul>
                    </div>

                </footer>
        </div>

    </body>

    </html>
