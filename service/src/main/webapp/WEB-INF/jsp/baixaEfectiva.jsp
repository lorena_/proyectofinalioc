<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
  <!DOCTYPE html>
  <html>

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/baixaEfectiva.css">
    <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
    <title>Fins aviat</title>
  </head>

  <body>

    <div class="menu-btn">
      <i class="fas fa-bars fa-2x"></i>
    </div>

    <div class="container">
      <!-- Header -->
      <header id="home-header">
        <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

        <nav class="main-nav">
          <ul class="main-menu">
            <li><a href="/">Home</a></li>
            <li><a href="quiSom">Qui Som</a></li>
            <li><a href="buscarReserva">Reservar tractament</a></li>
          </ul>
          <ul class="right-menu">
            <li><a href="login">Login</a></li>
            <li><a href="tipusUsuari">Registre</a></li>
          </ul>
        </nav>
      </header>

      <!--Main-->
      <main>

        <div class="container-main">

          <div class="baixa">
            <img src="/img/baixa-efectiva.jpg" alt="baixa">
          </div>

          <div class="baixa">
            <h3>Esperem que tornis aviat</h3>

          </div>

      </main>

      <!--Footer-->
      <footer id="main-footer" class="footer">
        <div class="footer-inner">
          <div><img src="/img/Dark-logo.png" class="logo"></div>
          <ul>
            <li><a href="#">Contacte</a></li>
            <li><a href="#">Politica de la Web</a></li>
            <li><a href="#">&copy; Beauty Service 2022</a></li>
          </ul>
        </div>

      </footer>

    </div>

  </body>

  </html>