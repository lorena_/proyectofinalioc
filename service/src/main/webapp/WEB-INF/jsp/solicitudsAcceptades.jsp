<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
        <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <!DOCTYPE html>
    <html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/benvingut.css">
        <script src="/js/home.js" type="text/javascript"></script>
        <link rel="stylesheet" href="/css/consentimentBanner.css">
        <script src="/js/cookies.js" type="text/javascript"></script>
        <script src="/js/consentimentBanner.js" type="text/javascript"></script>
        <link rel="shortcut icon" href="/img/Dark-Favicon.png" type="image/x-icon">
        <script src="https://kit.fontawesome.com/7c1b1fcd24.js" crossorigin="anonymous"></script>
        <title>Les meves solicituds</title>
    </head>

    <body>

        <!-- Responsive menu -->

        <div class="menu-btn">
            <i class="fas fa-bars fa-2x"></i>
        </div>

        <div class="container">
            <!-- Header -->
            <header id="home-header">
                <img src="/img/Light-logo.png" alt="Beauty-Service" class="logo">

                <nav class="main-nav">
                    <ul class="main-menu">
                        <li><a href="/">Home</a></li>
                        <li><a href="quiSom">Qui Som</a></li>
                        <li><a href="buscarReserva">Reservar tractament</a></li>
                    </ul>
                    <ul class="right-menu">
                        <li><a href="lesMevesDades.jsp">Hola ${nomUsuari}</a></li>
                        <li><i class="fa-solid fa-user"></i></li>
                        <li><a href="logout">Tancar sessio</a></li>
                    </ul>
                </nav>
            </header>

            <!-- Main area-->
            <main>

                <section class="main-section">

                    <!-- My Profile Menu -->

                    <section id="lateral-menu">
                        <div>
                            <nav class="main-menu">
                                <ul class="menuLateral">
                                    <a href="benvingut">Les meves solicituds pendents</a>
                                    <a href="elsMeusSalons">Els meus salons</a>
                                    <a href="lesMevesDades">Les meves dades</a>
                                    <a href="showRegistreSalo">Afegir un nou salo</a>
                                    <a href="showRegistreGestorTipusServei">Afegir un tipus de servei</a>
                                    <a href="baixa">Donar-te de baixa</a>
                                </ul>
                            </nav>
                        </div>
                    </section>

                    <!-- Responsive Profile Menu-->
                    <div class="profile-menu">
                        <button onclick="myFunction()" class="dropbtn">Les Meves Opcions</button>
                        <div id="myDropdown" class="dropdown-content">
                            <a href="benvingut">Les meves solicituds pendents</a>
                            <a href="elsMeusSalons">Els meus salons</a>
                            <a href="lesMevesDades">Les meves dades</a>
                            <a href="showRegistreSalo">Afegir un nou salo</a>
                            <a href="danarDeBaixaSalo">Donar de baixa Salo</a>
                            <a href="showRegistreGestorTipusServei">Afegir un tipus de servei</a>
                            <a href="baixa">Donar-te de baixa</a>
                        </div>
                    </div>

                    <!-- Reserves -->
                    <c:choose>
                        <c:when test="${fn:length(reserves) == '0'}">
                            <p>No tens cap solicitud acceptada en aquest salo</p><br>
                            <div><a href="elsMeusSalons" class="btn">Tornar</a></div>
                        </c:when>
                        <c:otherwise>
                            <section id="reserves-cards" class="reserves-cards">
                                <div class="reserves">
                                    <h3>Les meves solicituds acceptades</h3>
                                </div>
                                <div>
                                    <img src="/img/Sauna.jpg" alt="Sauna">
                                    <div class="card-content">
                                            <c:forEach items="${reserves}" var="reserves" varStatus="loop">
                                                <tr>
                                                    <h3 class="card-title">${reserves.text}</h3>
                                                </tr>
                                                <a href="detallReservaGestor?idSalo=${reserves.idSalo}&idReserva=${reserves.idReserva}" class="btn">Detall de la reserva</a>
                                            </c:forEach>                                                                                
                                      
                                    </div>
                                </div>
                        </c:otherwise>
                    </c:choose> 
                </section>

            </main>          

            <!--Footer-->
            <footer id="main-footer" class="footer">
                <div class="footer-inner">
                    <div><img src="/img/Dark-logo.png" class="logo"></div>
                    <ul>
                        <li><a href="#">Contacte</a></li>
                        <li><a href="#">Politica de la Web</a></li>
                        <li><a href="#">&copy; Beauty Service 2022</a></li>
                    </ul>
                </div>

            </footer>

        </div>

    </body>

    </html>