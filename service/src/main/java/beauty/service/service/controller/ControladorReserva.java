package beauty.service.service.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import beauty.service.service.repositori.Entitats.Event;
import beauty.service.service.repositori.Entitats.Reserves;
import beauty.service.service.repositori.Entitats.Salo;
import beauty.service.service.repositori.Entitats.Serveis;
import beauty.service.service.repositori.Entitats.TipusServeis;
import beauty.service.service.repositori.Entitats.Usuari;
import beauty.service.service.service.EventService;
import beauty.service.service.service.ReservaService;
import beauty.service.service.service.SaloService;
import beauty.service.service.service.ServeisService;
import beauty.service.service.service.TipusServeisService;
import beauty.service.service.service.UsuariService;

/**
 * Classe que representa el controlador dels mètodes relacionats amb una reserva,
 * tant per part de l'usuari gestor com del client.
 */
@Controller
public class ControladorReserva {

    @Autowired
    private TipusServeisService tipusServeisService;

    @Autowired
    private UsuariService usuariService;

    @Autowired
    private ServeisService serveisService;

    @Autowired
    private SaloService saloService;

    @Autowired
    private EventService eventService;

    @Autowired
    private ReservaService reservaService;
    
    /**
     * Mètode que recupera el llistat de tots els tipus de serveis de la base de dades 
     * i els introdueix en un desplegable. A més, si l'usuari està logat, automàticament
     * apareix cumplimentat el camp de codi postal. Si l'suari no està logat o no està registrat,
     * el camp de codi postal constarà buit per a què el puguin cumplimentar.
     * @param model
     * @param principal
     * @return vista "buscarReserva"
     */
    @GetMapping(path="/buscarReserva")
    public String buscarReserva(ModelMap model, Principal principal) {
        Iterable<TipusServeis> tipusServeis = tipusServeisService.findAllServeis();
        model.addAttribute("tipusServeis", tipusServeis);
        if (principal != null) {
            Usuari usuariLogat = usuariService.getUsuariByUsername(principal.getName());
            String missatgeLogat = "Hola " + usuariLogat.getNomUsuari();
            model.addAttribute("tancarSessio", "Tancar sessió");
            model.addAttribute("logat", missatgeLogat);
            model.addAttribute("codiPostal", usuariLogat.getCodiPostal());
        } else {
            model.addAttribute("login", "Login");
            model.addAttribute("registre", "Registre");
        }

        return "buscarReserva";
    }

    /**
     * Mètode que rep com a paràmetre l'identificador d'un TipusServeis, el recupera i retorna una vista
     * que ja té seleccionat aquest element. Si l'usuari està logat, també surt cumplimentat el seu codi postal.
     * @param idTractament
     * @param request
     * @param model
     * @param principal
     * @return
     */
    @GetMapping(path="/buscarEspecific")
    public String buscarEspecific(@RequestParam(value="nomTractament") String nomTractament, HttpServletRequest request, ModelMap model, Principal principal) {
        if(principal != null) {
            Usuari usuari = usuariService.getUsuariByUsername(principal.getName());
            model.addAttribute("nomUsuari", usuari.getNomUsuari());
            model.addAttribute("codiPostal", usuari.getCodiPostal());
        } 
        List<TipusServeis> allTipusServeis = tipusServeisService.findAllListTipusServeis();
        if (nomTractament.equals("Saunatradicional")) {
            request.setAttribute("nomTipusServei", "Sauna tradicional");
        }
        if (nomTractament.equals("TractamentDePellCarbonPeelFlash")) {
            request.setAttribute("nomTipusServei", "Tractament de pell Carbon Peel Flash");
        }
        if (nomTractament.equals("MaquillatgePerAFotos")) {
            request.setAttribute("nomTipusServei", "Maquillatge per a fotos");
        }
        if (nomTractament.equals("MassatgeReafirmantEstimulant")) {
            request.setAttribute("nomTipusServei", "Massatge Reafirmant Estimulant");
        }
     
        model.addAttribute("tipusServeis", allTipusServeis);

        return "buscarReserva";
    }

    /**
     * Mètode que recupera les dades de 'tipusServei' i 'codiPostal' introduïdes per l'usuari 
     * en la vista 'buscarReserva' i que fa diferents bucles sobre els llistats de serveis i salons,
     * cercant els que coincideixen amb els paràmetres esmentats i introduint-los en un llistat.
     * Si no hi ha coincidències, informa d'aquest fet i el mètode retorna la vista 'buscarReserva'.
     * Si troba resultats coincidents, retornarà una vista amb els resultats.
     * @param codiPostal
     * @param model
     * @param principal
     * @param request
     * @return vista "cercarReserva"
     */
    @GetMapping(path="/cercaReserva")
    public String cercaReserva(ModelMap model, Principal principal, HttpServletRequest request) {
        String codiPostal = request.getParameter("codiPostal");
        String nomTipusServei = request.getParameter("tipusServeis");
        Iterable<Serveis> allServeis = serveisService.findAllServeis();
        List<Salo> salonsAmbElServei = new ArrayList<>();
        for(Serveis servei: allServeis) {
            if (servei.getNomTipusServei().equals(nomTipusServei)) {
                Salo salo = saloService.getSaloById(servei.getIdSalo());
                salonsAmbElServei.add(salo);
            }
        }
        List<Salo> salonsAmbServeiCp = new ArrayList<>();
        if(salonsAmbElServei.size() != 0) {
            for(Salo s: salonsAmbElServei) {
                if(s.getCodiPostal().equals(codiPostal)) {
                    salonsAmbServeiCp.add(s);
                }
            }
        }
        

        if(salonsAmbServeiCp.size() == 0) {
            model.addAttribute("capSalo", "No hi ha cap saló que ofereixi aquest servei en aquesta  zona");
            Iterable<TipusServeis> tipusServeis = tipusServeisService.findAllServeis();
            model.addAttribute("tipusServeis", tipusServeis);

            return "buscarReserva";
        }
        model.addAttribute("salons", salonsAmbServeiCp);
        if(principal != null) {
            model.addAttribute("nomUsuari", principal.getName());
            model.addAttribute("tipusServei", nomTipusServei);
        } else {
            model.addAttribute("usuariLogat", "no");
        }
        
        return "cercaReserva";
    }

    /**
     * Mètode que recull les dades introduides en la recerca de servei i l'identificador
     * del saló seleccionat pel client i retorna una vista amb un calendari que té la informació
     * de saló i del servei que vol reservar i amb el qual el client pot seleccionar un dia i una hora. ç
     * A més, té un enllaç per confirma la sol·licitud de reserva i que es quedi guardada a la base de 
     * dades.
     * @param idSalo
     * @param tipusServei
     * @param model
     * @param http
     * @param principal
     * @return vista "calendariClient"
     */
    @GetMapping(path="/calendariClient")
    public String calendariCitesSaloProva(@RequestParam(value="idSalo") int idSalo, @RequestParam(value="tipusServei")
        String tipusServei, ModelMap model, HttpServletRequest http, Principal principal) {

        model.addAttribute("nomUsuari", principal.getName());
        model.addAttribute("tipusServei", tipusServei);
        model.addAttribute("idSalo", idSalo);

        return "calendariClient";
    }

    /**
     * Mètode que retorna un resum de les dades de la solicitud de reserva que es genera
     * quan el client selecciona una data i hora al calendari. Abans, processa la informació enviada
     * de l'Event del calendari i crea un nou objecte Reserva que guarda a la base de dades un registre
     * amb l'id de l'event, del client, del saló i del tipus de servei reservat. A més, automàticament 
     * li dóna valor 0 al seu estat (pendent de tramitar per part del gestor)
     * @param idEvent
     * @param idSalo
     * @param model
     * @param principal
     * @return vista "resumReserva"
     */
    @GetMapping(path="/resumReserva")
    public String resumReserva(@RequestParam(value="idEvent") int idEvent, @RequestParam(value="idSalo") int idSalo,
        ModelMap model, Principal principal) {
        Event event = eventService.getEventById(idEvent);
        Usuari client = usuariService.getUsuariByUsername(principal.getName());
        Salo salo = saloService.getSaloById(idSalo);
        Reserves reserva = new Reserves();
        reserva.setStart(event.getStart());
        reserva.setEnd(event.getEnd());
        reserva.setEstat(0);
        reserva.setIdEvent(idEvent);
        reserva.setIdSalo(idSalo);
        reserva.setText(event.getText());
        reserva.setIdUsuari(client.getIdUsuari());
        reservaService.guardarReserva(reserva);
        
        model.addAttribute("idUsuari", client.getIdUsuari());
        model.addAttribute("nomUsuari", client.getNomUsuari());
        model.addAttribute("salo", salo.getNomSalo());
        model.addAttribute("data", reserva.getStart());
        model.addAttribute("estat", reserva.getEstat());
        model.addAttribute("servei", reserva.getText());
        model.addAttribute("idReserva", reserva.getIdReserva());

        return "resumReserva";
    }

    /**
     * Mètode que recupera la Reserva amb el seu id passat com a paràmetre i el saló en el qual
     * s'ha reservat aquesta també amb l'id passat com a paràmetre. Després, envia totes les dades
     * de la reserva a una vista que mostrarà tota la informació de la reserva.
     * @param idReserva
     * @param idSalo
     * @param model
     * @param principal
     * @return vista "detallReservaClient"
     */
    @GetMapping(path="/detallReservaClient")
    public String detallReservaClient(@RequestParam(value="idReserva") int idReserva, @RequestParam(value="idSalo") int idSalo, 
            ModelMap model, Principal principal) {
        Usuari client = usuariService.getUsuariByUsername(principal.getName());
        Reserves reserva = reservaService.getReservaById(idReserva);
        Salo salo = saloService.getSaloById(idSalo);
        
        model.addAttribute("idUsuari", client.getIdUsuari());
        model.addAttribute("nomUsuari", client.getNomUsuari());
        model.addAttribute("salo", salo.getNomSalo());
        model.addAttribute("idReserva", reserva.getIdReserva());
        model.addAttribute("data", reserva.getStart());
        model.addAttribute("estat", reserva.getEstat());
        model.addAttribute("servei", reserva.getText());

        return "detallReservaClient";
    }

    /**
     * Mètode que rep com a paràmetre l'identificador de la Reserva i l'identificador del saló i recupera
     * les dues instàncies mitjançant mètodes del repositori. Introdueix les dades en una vista i ho retorna
     * a l'usuari gestor per a què aquest pugui veure amb detall les dades de la reserva.
     * @param idReserva
     * @param idSalo
     * @param model
     * @param principal
     * @return "detallVistaGestor"
     */
    @GetMapping(path="/detallReservaGestor")
    public String detallReservaGestor(@RequestParam(value="idReserva") int idReserva, @RequestParam(value="idSalo") int idSalo, 
            ModelMap model, Principal principal) {
        Usuari gestor = usuariService.getUsuariByUsername(principal.getName());
        Reserves reserva = reservaService.getReservaById(idReserva);
        Usuari client = usuariService.getUsuariById(reserva.getIdUsuari()); 
        Salo salo = saloService.getSaloById(idSalo);

        model.addAttribute("nomUsuari", gestor.getNomUsuari());
        model.addAttribute("servei", reserva.getText());
        model.addAttribute("salo", salo.getNomSalo());
        model.addAttribute("data", reserva.getStart());
        model.addAttribute("nomClient", client.getNom());
        model.addAttribute("idSalo", idSalo);

        return "detallReservaGestor";
    }

    /**
     * Mètode que recupera, per una banda, l'usuari logat, i per l'altre, totes les reserves que ha fet
     * l'usuari separades en diferents llistats depenent del seu estat. Després introduiex aquestes dades
     * una vista que es retorna al client per a què pugui veure les reserves separades segons estat (pendents
     * de tramitar, acceptades o rebutjades). A més, totes seran de data posterior a l'actual del sistema
     * @param model
     * @param principal
     * @return vista 'lesMevesReserves'
     */
    @GetMapping(path="/lesMevesReserves")
    public String lesMevesReserves(ModelMap model, Principal principal) {
        Usuari client = usuariService.getUsuariByUsername(principal.getName());
        List<Reserves> reservesClient = reservaService.getReservesByIdClient(client.getIdUsuari());
        List<Reserves> reservesConfirmades = reservaService.getReservesByEstat(reservesClient, 1);
        List<Reserves> reservesPendents = reservaService.getReservesByEstat(reservesClient, 0);
        List<Reserves> reservesDescartades = reservaService.getReservesByEstat(reservesClient, 2);

        model.addAttribute("idUsuari", client.getIdUsuari());
        model.addAttribute("nomUsuari", client.getNomUsuari());
        model.addAttribute("reservesConfirmades", reservesConfirmades);
        model.addAttribute("reservesPendents", reservesPendents);
        model.addAttribute("reserves", reservesClient);
        model.addAttribute("reservesDescartades", reservesDescartades);
        
        return "lesMevesReserves";
    }

    /**
     * Mètode que rep com a paràmetre l'identificador del saló i que recupera, mitjançant un mètode del
     * repositori, totes les reserves realitzades en ell. Després les filtra per obtenir només les que es troben
     * en estat 0 (pendents) i les introdueix a la vista que retorna el mètode.
     * @param idSalo
     * @param model
     * @param principal
     * @return vista 'solicitudsPendents'
     */
    @GetMapping(path="/solicitudsPendents")
    public String solicitudsPendents(@RequestParam(value="idSalo") int idSalo, ModelMap model, Principal principal) {
        Usuari gestor = usuariService.getUsuariByUsername(principal.getName());
        List<Reserves> reservesSalons = new ArrayList<>();
        List<Reserves> allReserves = reservaService.allReserves();
        for(Reserves r: allReserves) {
            if(r.getIdSalo() == idSalo) {
                reservesSalons.add(r);
            }
        }
        List<Reserves> reservesPendents = new ArrayList<>();
        for(Reserves r: reservesSalons) {
            if(r.getEstat()==0) {
                reservesPendents.add(r);
            }
        }

        model.addAttribute("reserves", reservesPendents);
        model.addAttribute("nomUsuari", gestor.getNomUsuari());

        return "solicitudsPendents";
    }
    
    /**
     * Mètode que rep com a paràmetre els identificadors de la reserva i el saló i recupera les dues instàncies.
     * Introdueix les dades a una vista que conté dos botons que permetran a l'usuari de tipus gestor interactuar
     * amb la reserva i acceptar-la o rebutjar-la. 
     * @param idSalo
     * @param idReserva
     * @param model
     * @param principal
     * @return vista 'tramitacioSolicitud'
     */
    @GetMapping(path="/tramitacioSolicitud")
    public String tramitacioSolicitud(@RequestParam(value="idSalo") int idSalo, @RequestParam(value="idReserva") int idReserva,
        ModelMap model, Principal principal) {
            Usuari gestor = usuariService.getUsuariByUsername(principal.getName());
            Reserves reserva = reservaService.getReservaById(idReserva);
            Usuari client = usuariService.getUsuariById(reserva.getIdUsuari());
            Salo salo = saloService.getSaloById(idSalo);

            model.addAttribute("nomUsuari", gestor.getNomUsuari());
            model.addAttribute("data", reserva.getStart());
            model.addAttribute("estat", reserva.getEstat());
            model.addAttribute("nom", client.getNom());  
            model.addAttribute("nomSalo", salo.getNomSalo());
            model.addAttribute("idSalo", idSalo);
            model.addAttribute("servei", reserva.getText());
            model.addAttribute("idReserva", reserva.getIdReserva());

            return "tramitacioSolicitud";
    }

    /**
     * Mètode que rep com a paràmetre els identificadors de la Reserva i del Saló i també un String
     * que representa la resposta de l'usuari gestor a la reserva ("ok" o "ko"). Amb aquestes dades
     * s'encarrega de canviar l'estat d'aquesta, és a dir, de tramitar-la. A la vista que retorna es 
     * podrà veure reflexat el resultat de la seva interacció
     * @param idSalo
     * @param idReserva
     * @param estat
     * @param model
     * @param principal
     * @return 'tramitacioSolicitudOK'
     */
    @GetMapping(path="/tramitacioSolicitudOK") 
    public String tramitacioSolicitudOK(@RequestParam(value="idSalo") int idSalo, @RequestParam(value="idReserva") int idReserva, 
            @RequestParam(value="estat") String estat, ModelMap model, Principal principal) {
        Usuari gestor = usuariService.getUsuariByUsername(principal.getName());
        Reserves reserva = reservaService.getReservaById(idReserva);
        Usuari client = usuariService.getUsuariById(reserva.getIdUsuari()); 
        if(estat.equals("ok")) {
            reserva.setEstat(1);
        } else {
            reserva.setEstat(2);
            model.addAttribute("nomUsuari", gestor.getNomUsuari());
            model.addAttribute("idSalo", idSalo);
            model.addAttribute("data", reserva.getStart());
            model.addAttribute("nom", client.getNom());
            model.addAttribute("missatge", "Has descartat la reserva");

            return "tramitacioSolicitudOK";
        }
        
        model.addAttribute("nomUsuari", gestor.getNomUsuari());
        model.addAttribute("idSalo", idSalo);
        model.addAttribute("data", reserva.getStart());
        model.addAttribute("nom", client.getNom());
        model.addAttribute("missatge", "Has acceptat la reserva");

        return "tramitacioSolicitudOK";
    }

    /**
     * Mètode que rep com a paràmetre l'identificador del saló i que recupera, mitjançant un mètode del
     * repositori, totes les reserves realitzades en ell. Després les filtra per obtenir només les que es troben
     * en estat 1 (acceptades) i les introdueix a la vista que retorna el mètode.
     * @param idSalo
     * @param model
     * @param principal
     * @return vista 'solicitudsAcceptades'
     */
    @GetMapping(path="/solicitudsAcceptades")
    public String solicitudsAcceptades(@RequestParam(value="idSalo") int idSalo, ModelMap model, Principal principal) {
        Usuari gestor = usuariService.getUsuariByUsername(principal.getName());
        List<Reserves> reservesSalons = new ArrayList<>();
        List<Reserves> allReserves = reservaService.allReserves();
        for(Reserves r: allReserves) {
            if(r.getIdSalo() == idSalo) {
                reservesSalons.add(r);
            }
        }
        List<Reserves> reservesAcceptades = new ArrayList<>();
        for(Reserves r: reservesSalons) {
            if(r.getEstat()==1) {
                reservesAcceptades.add(r);
            }
        }

        model.addAttribute("reserves", reservesAcceptades);
        model.addAttribute("nomUsuari", gestor.getNomUsuari());

        return "solicitudsAcceptades";
    }

    /**
     * Mètode que rep com a paràmetre l'identificador de la Reserva i l'utilitza per recuperar-la 
     * mitjançant un mètode del repositori i per canviar el seu estat a 'eliminada' (estat 3)
     * @param idReserva
     * @param model
     * @param principal
     * @return redirecció a la vista 'lesMevesReserves'
     */
    @GetMapping("/eliminarSolicitudClient")
    public String eliminarSolicitudClient(@RequestParam(value="idReserva") int idReserva, ModelMap model, 
        Principal principal) {
            Usuari usuari = usuariService.getUsuariByUsername(principal.getName());
            model.addAttribute("nomUsuari", usuari.getNomUsuari());
            Reserves reserva = reservaService.getReservaById(idReserva);
            reserva.setEstat(3);

            return "redirect:/lesMevesReserves";
    }
    
    /**
     * Mètode que rep com a paràmetre l'identificador d'usuari. Amb aquest, recupera totes les seves reserves
     * que es troben en data anterior a l'actual del sistema. Després les introdueix en una vista que les
     * mostrarà al client.
     * @param idUsuari
     * @param model
     * @param principal
     * @return vista 'historialReserves'
     */
    @GetMapping("/historialReserves")
    public String historialReserves(@RequestParam(value="idUsuari") int idUsuari, ModelMap model, Principal 
        principal) {
        List<Reserves> reservesAntigues = reservaService.getReservesAntiguesByClient(idUsuari);

        model.addAttribute("nomUsuari", principal.getName());
        model.addAttribute("reserves", reservesAntigues);

        return "historialReserves";
    }

    /**
     * Mètode que rep com a paràmetre l'identificador de la reserva i del saló. D'aquesta manera, recupera
     * les dades de la reserva antiga que ha seleccionat el client i li permet veure el detall d'aquesta.
     * @param idReserva
     * @param idSalo
     * @param model
     * @param principal
     * @return vista 'detallReservaAntiga'
     */
    @GetMapping("/detallReservaHistorial")
    public String detallReservaHistorial(@RequestParam(value="idReserva") int idReserva, @RequestParam(value="idSalo") int idSalo, 
    ModelMap model, Principal principal) {
        Reserves reserva = reservaService.getReservaById(idReserva);
        Salo salo = saloService.getSaloById(idSalo);
        Usuari client = usuariService.getUsuariByUsername(principal.getName());

        model.addAttribute("servei", reserva.getText());
        model.addAttribute("salo", salo.getNomSalo());
        model.addAttribute("data", reserva.getStart());
        model.addAttribute("nomUsuari", client.getNomUsuari());
        model.addAttribute("idUsuari", client.getIdUsuari());

        return "detallReservaAntiga";
    }
    
    /**
     * Mètode que rep com a paràmetre l'identificador de la reserva i li assigna a aquesta l'estat 3 (eliminada).
     * @param idReserva
     * @param model
     * @param principal
     * @return vista 'reservaCancelada'
     */
    @GetMapping("/cancelarSolicitud")
    public String cancelarReserva(@RequestParam(value="idReserva") int idReserva, ModelMap model, Principal principal) {
        Usuari usuari = usuariService.getUsuariByUsername(principal.getName());
        model.addAttribute("nomUsuari", usuari.getNomUsuari());
        Reserves reserva = reservaService.getReservaById(idReserva);
        reserva.setEstat(3);
        
        return "reservaCancelada";
    } 

    
    
    /** 
     * Funció que retorna una vista per mostrar un calendari per reservar cites
     * @return String jsp de calendari de cites
     */
    @GetMapping("/calendariCitesSalo")
    public String calendariCitesSalo() {

        
        return "calendariCitesSalo";
    }
    
}