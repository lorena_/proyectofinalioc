
package beauty.service.service.controller;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import beauty.service.service.repositori.CrudRepositoriReserves;
import beauty.service.service.repositori.CrudRepositoriServeis;
import beauty.service.service.repositori.CrudRepositoriUsuari;
import beauty.service.service.repositori.EventRepository;
import beauty.service.service.repositori.RepositoriServeis;
import beauty.service.service.repositori.Entitats.Event;
import beauty.service.service.repositori.Entitats.Reserves;
import beauty.service.service.repositori.Entitats.Serveis;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@RestController
public class ControladorEvent {

    @Autowired
    EventRepository er;

    @Autowired
    CrudRepositoriServeis crudRepositoriServeis;

    @Autowired
    private RepositoriServeis RepositoriServeis;

    @Autowired
    private CrudRepositoriReserves crudRepositoriReserves;

    @Autowired
    CrudRepositoriUsuari crudRepositoriUsuari;

    
    
    /** 
     *<p> Funció que mostra tots els events a partir de dos dates donades</p>
     * @param ISO.DATE_TIME Es passa la data a un formato estandar
     * @return Iterable<Event> Es mostra una lista de tots el events a partir de una data de inici i altre de fi
     */
    @GetMapping("/api/events")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    Iterable<Event> events(@RequestParam("start") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime start,
            @RequestParam("end") @DateTimeFormat(iso = ISO.DATE_TIME) LocalDateTime end) {
        return er.findBetween(start, end);
    }

    
    /** 
     * Funció que crea i retorne cites a partir de parametres rebuts
     * @param params Objectes que es rebren desde el client per crear un event
     * @return Event Retorna un objetecte de tipus Event
     */
    @PostMapping("/api/events/create")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Transactional
    Event createEvent(@RequestBody EventCreateParams params) {

        Event e = new Event();
        e.setStart(params.start);
        e.setEnd(params.end);
        e.setText(params.text);
        e.setText2(params.text2);
        er.save(e);

        return e;
    }

    
    /** 
     * Funció que crea i retorne cites a partir de parametres rebuts
     * @param params Objectes que es rebren desde el client per crear una rereserva
     *  @return ResponseEntity<Reserves> Retorna una entitat de tipus Reserves
     */

    @PostMapping("/api/events/createReserves")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Transactional
    public ResponseEntity<Reserves> save(@RequestBody @Valid EventCreateParams params) {
        Reserves reserves = new Reserves();

        reserves.setStart(params.start);
        reserves.setEnd(params.end);
        reserves.setText(params.text);

        final String currentUserName = SecurityContextHolder.getContext().getAuthentication().getName();

        int currentUserNameId = crudRepositoriUsuari.getIdUsuariByUsername(currentUserName);
        if (currentUserNameId >= 0) {
            reserves.setIdUsuari(currentUserNameId);
        }

        crudRepositoriReserves.save(reserves);

        return new ResponseEntity<Reserves>(reserves, HttpStatus.OK);

    }

    
    /** 
     * Mètode afegeix i retorna un objecte de tipus event quan l'usuari arrosega un event, tot aixó a partir de parametres rebuts
     * @param params Parametre de tipus objectes 
     * @return Event Retorna un objecte de tipus Event
     */
    @PostMapping("/api/events/move")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Transactional
    Event moveEvent(@RequestBody EventMoveParams params) {

        Event e = er.findById(params.id).get();
        e.setStart(params.start);
        e.setEnd(params.end);

        er.save(e);

        return e;
    }

    
    /** 
     * Funció que cambia el formato de color de cada celda de rang seleccionat en una data dins el calenadri
     * @param params paràmetre de tipus objecte per cambiar el color de cada celda d'un calendari
     * @return Event Retorna un objecte de tipus Event
     */
    @PostMapping("/api/events/setColor")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Transactional
    Event setColor(@RequestBody SetColorParams params) {

        Event e = er.findById(params.id).get();
        e.setColor(params.color);
        er.save(e);

        return e;
    }

    
    /** 
     * Funció que esborra un Event, a partir de un objecte id
     * @param params Paràmetre id de tipus objecte per poder eliminat un Event
     * @return EventDeleteResponse Retorna un objecte de tipus EventDeleteResponse, per notificar que un Event ha sigut eliminat
     */
    @PostMapping("/api/events/delete")
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Transactional
    EventDeleteResponse deleteEvent(@RequestBody EventDeleteParams params) {

        er.deleteById(params.id);

        return new EventDeleteResponse() {
            {
                message = "Deleted";
            }
        };
    }

    
    /** 
     * Funció que retorna un listat de tipus de serveis 
     * @param request Petició per obtenir el nom d'un saló a partir d'una sessió oberta
     * @return List<Serveis> Llistat de serveis
     */
    @GetMapping(value = "/api/events/TipusServeis")
    public List<Serveis> getResource(HttpServletRequest request) {

        HttpSession session = request.getSession();
        List<Serveis> listServeisDisponible = (List<Serveis>) session.getAttribute("serveisSalo");

        return listServeisDisponible;
    }

    
    /** 
     * Mètode que retorna un tipus de servie a partir un id donat
     * @param id El id per mostrar un tipus servie
     * @return List<Serveis> Llistat de tipus de serevei
     */
    @GetMapping(value = "/api/events/TipusServeisByIdSalo/{id}")
    public List<Serveis> getTipusServeis(@PathVariable int id) {

        List<Serveis> tipusServeis = RepositoriServeis.getServeisByIdSalo(id);

        return tipusServeis;
    }

    /**
     Classe predeterminada per eliminar un Envent a parit d'un id
     */
    public static class EventDeleteParams {
        public Long id;
    }

        /**
      Classe predeterminada per mostrar un missatge després d'eliminar un Event
     */
    public static class EventDeleteResponse {
        public String message;
    }

    /**
     Classe per dafault per passar per paràmetre  com un objete de tipus "EventCreateParams"
     */
    public static class EventCreateParams {
        public LocalDateTime start;
        public LocalDateTime end;
        public String text;
        public String text2;
        public Long resource;
    }

/**
 Classe predeterminada per passar per paràmetre  com un objete de tipus "EventMoveParams",
 per actulizar un Envent quan s'arrosega una data dins el calendari
 */
    public static class EventMoveParams {
        public Long id;
        public LocalDateTime start;
        public LocalDateTime end;
        public Long resource;
    }

/**
 *Classe predeterminada per passar per paràmetre  com un objete de tipus "SetColorParams",
 per cambiar el color de cada celda d'un calendari
 */
    public static class SetColorParams {
        public Long id;
        public String color;
    }

}