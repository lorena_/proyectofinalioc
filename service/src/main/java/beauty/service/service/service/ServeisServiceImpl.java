package beauty.service.service.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import beauty.service.service.repositori.CrudRepositoriServeis;
import beauty.service.service.repositori.RepositoriServeis;
import beauty.service.service.repositori.Entitats.Serveis;


/**
 * Classe per implementar un servei i poder gestionar servies d'un salo
 */
@Service
public class ServeisServiceImpl implements ServeisService {

    @Autowired
    CrudRepositoriServeis crudRepositoriServeis;

    @Autowired
    RepositoriServeis repositoriServeis;

    
    /** 
     * @return Iterable<Serveis>
     */
    @Override
    public Iterable<Serveis> findAllServeis() {

        return crudRepositoriServeis.findAll();
    }

    
    /** 
     * Funció que pot torna o no un objete tipus Servies trobant-ho a partir d'un objecte
     * tipus Serveis.
     * @param serveis Objecte tipus Servies a trobar.
     * @return Optional<Serveis> Pot retornar o no un objecte tipus Serveis, passan-li un objecte tipus
     * Serveis.
     */
    @Override
    public Optional<Serveis> findByIdServeis(Serveis serveis) {
        return crudRepositoriServeis.findById(serveis.getIdServei());
    }

    
    /** 
     * Mèdode que gestiona la persistencia de un Servei, a partir d'un objecte del mateix tipus.
     * @param serveis Objete a guardar.
     * @return Serveis Retorna un objete Servei.
     */
    @Override
    public Serveis guardarServeis(Serveis serveis) {
        return crudRepositoriServeis.save(serveis);
    }

    
    /** 
     * Funció que esborra un objecte tipus Serveis passan-li un objecte del mateix tipus.
     * @param serveis Objete Serveis a esborrar.
     */
    @Override
    public void deleteServeis(Serveis serveis) {
        crudRepositoriServeis.delete(serveis);        
    }

    
    /** 
     * Funció que obté un llistat de Servies des de un id de saló.
     * @param idSalo Id del saló a llistar.
     * @return List<Serveis> Llista de objestes tipus Serveis.
     */
    @Override
    public List<Serveis> getServeisByIdSalo(int idSalo) {
        return repositoriServeis.getServeisByIdSalo(idSalo);
    }
    
    
    /** 
     * Mètode per esborrar un salo a partir de un id.
     * @param idSalo Id del saló a esborrar.
     */
    @Override
    public void deleteServeisSalo(int idSalo) {
        repositoriServeis.deleteServeisSalo(idSalo);
    }

    
}
