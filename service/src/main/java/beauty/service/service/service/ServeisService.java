package beauty.service.service.service;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import beauty.service.service.repositori.Entitats.Serveis;



@Service
public interface ServeisService {
    Iterable<Serveis> findAllServeis();
	Optional<Serveis> findByIdServeis(Serveis serveis);
	Serveis guardarServeis (Serveis serveis);
	void deleteServeis(Serveis serveis);
	List<Serveis> getServeisByIdSalo(int idSalo);
	void deleteServeisSalo(int idSalo);
}
