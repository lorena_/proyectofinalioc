package beauty.service.service.service;

import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;

import beauty.service.service.repositori.Entitats.Serveis;
import beauty.service.service.repositori.Entitats.TipusServeis;

/**
 * Interface que gestiona els tipus de serveis.
 */
@Service
public interface TipusServeisService {
    List<TipusServeis> findAllListTipusServeis();
    Iterable<TipusServeis> findAllServeis();
	Optional<TipusServeis> findByIdServeis(TipusServeis tipusServeis);
	TipusServeis guardarServeis (TipusServeis tipusServeis);
	void deleteServeis(TipusServeis tipusServeis);
    TipusServeis findTipusServeiByNom(String nom);
    List<Integer> llistatIdTipusServeis(List<Serveis> serveis);
    List<TipusServeis> llistatTipusServeisNoEscollits(List<Integer> serveisEscollits, List<TipusServeis> allTipusServeis);
    List<TipusServeis> findTipusServeisAndId();
    public TipusServeis getTipusServeiById(int idTipusServeis);
}
