package beauty.service.service.service;

import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import beauty.service.service.repositori.CrudRepositoriUsuari;
import beauty.service.service.repositori.RepositoriUsuari;
import beauty.service.service.repositori.Entitats.Usuari;

@Service
@Transactional
public class UsuariService {
    
    @Autowired
    private CrudRepositoriUsuari repositoriCrud;

    @Autowired
    private RepositoriUsuari repositori;

    //Mètode que retorna tots els usuaris de la BD
    public Iterable<Usuari> getAllUsuaris() {
        return repositoriCrud.findAll();
    }

    //Mètode que guarda un usuari a la BD
    public void guardaUsuari(Usuari usuari) {
        repositoriCrud.save(usuari);
    }

    //Mètode que retorna un usuari pel seu email
    public Usuari getUsuariByEmail(String email) {
        return repositori.getUsuariByMail(email);
    }

    //Mètode que retorna un usuari pel seu ID
    public Usuari getUsuariById(int id) {
        return repositori.getUsuariById(id);
    }

    //Mètode que retorna un usuari pel seu nomUsuari
    public Usuari getUsuariByUsername(String username) {
        return repositori.getUsuariByUsername(username);
    }

            //Mètode que desactiva a un usuari tipus GESTOR
            public void danarDeBaixaSalo(String userName){
                repositori.danarDeBaixaSalo(userName);
           }

           //Mètode que comprova a un usuari tipus GESTOR i no esta donat de baixa
           public boolean isUsuariGestorActive(String userName){
            return repositori.isUsuariGestorActive(userName);

           }
   
}
