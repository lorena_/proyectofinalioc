package beauty.service.service.repositori;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;
import org.springframework.stereotype.Repository;

import beauty.service.service.repositori.Entitats.Reserves;

/**
 * Interfices de de l'Entitat Reserves
 */
 
@Repository
public interface CrudRepositoriReserves extends CrudRepository<Reserves, Integer> {
	@Query("from Reserves e where not(e.end < :from or e.start > :to)")
	public List<Reserves> 
	findBetween(@Param("from") @DateTimeFormat(iso=ISO.DATE_TIME) LocalDateTime start, @Param("to") @DateTimeFormat(iso=ISO.DATE_TIME) LocalDateTime end);
}
