package beauty.service.service.repositori.Entitats;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Classe que representa una entitat de tipus Event. Es fa servir per guardar
 * cites a partir de una data de inici
 * i altre de fi.
 */
@Entity
public class Event {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long id;

	String text;

	String text2;

	LocalDateTime start;

	LocalDateTime end;

	String color;

	/**
	 * Funció re retorna un String.
	 * 
	 * @return String String a retornar.
	 */
	public String getText2() {
		return this.text2;
	}

	/**
	 * Mèdtode que actualitza text2
	 * 
	 * @param text2 Estring a actulitzar
	 */
	public void setText2(String text2) {
		this.text2 = text2;
	}

	/**
	 * Funció que ens retorna un id d'un Event
	 * 
	 * @return Long id de tipus Long
	 */
	public Long getId() {
		return id;
	}

	/**
	 * Mètode que actualitza l'id d'un Event
	 * 
	 * @param id Identificar a partir del qual s'actulitza un Event
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * Funció que opté un text de un Event.
	 * 
	 * @return String Text a retornar.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Mètode que actualitza la variabale text
	 * 
	 * @param text Valor de tipus String per actualitzar la variable text
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Funció que opté la data i temps local
	 * 
	 * @return LocalDateTime Retorna la data de inici local
	 */
	public LocalDateTime getStart() {
		return start;
	}

	/**
	 * Funció per actualitzat una data de inici
	 * 
	 * @param start Valor de tipus LocalDateTime a actulitzar
	 */
	public void setStart(LocalDateTime start) {
		this.start = start;
	}

	/**
	 * Mètode per obtenir la data i l'hora local de inici per poder escollir un
	 * servei desde el calendari
	 * 
	 * @return LocalDateTime Ens retorna la dada de inici a l'hora de treure un
	 *         servei
	 */
	public LocalDateTime getEnd() {
		return end;
	}

	/**
	 * Funció per actualitzar la data de inici de un servei.
	 * 
	 * @param end Data i hora lacal per poder agafar una cita d'un servie triat.
	 */
	public void setEnd(LocalDateTime end) {
		this.end = end;
	}

	/**
	 * Mètode per poder obtenir un color, que poden ser:
	 * <ul>
	 * <li>Groc
	 * <li>Verd
	 * <li>vermell
	 * <li>Blau
	 * </ul>
	 * 
	 * @return String Ens retorna el nom d'un color amb un valor tipus String.
	 */
	public String getColor() {
		return color;
	}

	/**
	 * Funció que actulitza un color de la celda seleccionada en el calendari per
	 * truere cites, i poder guardar-la en
	 * la base de dades.
	 * 
	 * @param color Amb aquest paràmetre de tipus String determina el color de cada
	 *              celda en el calendari
	 */
	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * Constructor buit de la classe Event
	 */
	public Event() {
	}

}
