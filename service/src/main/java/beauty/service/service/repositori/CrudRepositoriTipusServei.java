package beauty.service.service.repositori;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import beauty.service.service.repositori.Entitats.TipusServeis;

/**Interfície que crida a CrudRepository */
@Repository
public interface CrudRepositoriTipusServei extends CrudRepository<TipusServeis, Integer> {

    @Query("select x from TipusServeis x where x.nomTipusServei like :var_param")
    public TipusServeis getTipusServeiByNom(@Param("var_param") String nomTipusServei);
    
}
