package beauty.service.service.repositori;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import beauty.service.service.repositori.Entitats.Serveis;

/**
 * Classe que representa el repositori de la classe Serveis
 */
@Repository
public class RepositoriServeis {

    @Autowired
    private CrudRepositoriServeis crudRepositoriServeis;

    /**
     * Mètode que rep com a paràmetre l'identificador del saló i busca a la taula
     * Serveis de la base de dades un llistat dels serveis que té associats aquest saló
     * @param idSalo
     * @return llistat de serveis
     */
    public List<Serveis> getServeisByIdSalo(int idSalo) {
        Iterable<Serveis> serveis = crudRepositoriServeis.findAll();
        List<Serveis> serveisUsuari = new ArrayList<>();
        for(Serveis s: serveis) {
            if(s.getIdSalo() == idSalo) {
                serveisUsuari.add(s);
            }
        }
        return serveisUsuari;
    }

    /**
     * Mètode que rep com a paràmetre l'identificador d'un saló, recupera tots els
     * serveis que té associats a la taula Serveis i els esborra
     * @param idSalo
     */
    public void deleteServeisSalo(int idSalo) {
        Iterable<Serveis> allServeis = crudRepositoriServeis.findAll();
        for(Serveis servei : allServeis) {
            if(servei.getIdSalo() == idSalo) {
                crudRepositoriServeis.delete(servei);
            }
        }
    }
}
