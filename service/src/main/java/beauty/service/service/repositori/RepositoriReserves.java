package beauty.service.service.repositori;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import beauty.service.service.repositori.Entitats.Reserves;

/**
 * Classe que representa el repositori de la classe Reserves
 */
@Repository
public class RepositoriReserves {
    
    @Autowired
    private CrudRepositoriReserves crudRepositoriReserves;

    /**
     * Mètode que crida un mètode de la interfície CrudRepositoriReserves
     * que retorna totes les Reserves de la base de dades
     * @return Retorna una lista iterable de tots les reserves
     */
    public Iterable<Reserves> getAllReserves() {
        return crudRepositoriReserves.findAll();
    }
    
    /**
     * Mètode que rep com a paràmetre l'identificador de la reserva i la retorna d'entre 
     * totes les reserves guardades a la base de dades
     * @param idReserva Id que es fa servir per trobar una reserva
     * @return la reserva o null si no la troba
     */
    public Reserves getReservaById(int idReserva) {
        Iterable<Reserves> allReserves = getAllReserves();
        for(Reserves r: allReserves) {
            if(r.getIdReserva() == idReserva) {
                return r;
            }
        }
        
        return null;
    }

    /**
     * Mètode que rep com a paràmetre l'identificador d'un saló y que retorna un llistat amb
     * totes les reserves que s'han realitzat en aquest saló
     * @param idSalo Id per obtenir una reserva desde id d'un Salo
     * @return List de reserves que corresponen al saló
     */
    public List<Reserves> getReservesByIdSalo(int idSalo) {
        Iterable<Reserves> allReserves = crudRepositoriReserves.findAll();
        List<Reserves> reservesBySalo = new ArrayList<>();
        for(Reserves r: allReserves) {
            if(r.getIdSalo() == idSalo) {
                reservesBySalo.add(r);
            }
        }

        return reservesBySalo;
    }

    /**
     * Mètode que rep com a paràmetre l'identificador d'un client. Primer recupera totes les reserves 
     * que pertanyen a aquest client i després filtra les reserves que siguin de data posterior a l'actual
     * del sistema. 
     * @param idClient Identificar per poder trobar la reserva a partir de l'id d'un client
     * @return llistat de reserves que pertanyen al client i de data posterior a la data actual
     */
    public List<Reserves> getReservesByIdClient(int idClient) {
        Iterable<Reserves> allReserves = crudRepositoriReserves.findAll();
        List<Reserves> reservesByClient = new ArrayList<>();
        for(Reserves r: allReserves) {
            if(r.getIdUsuari() == idClient) {
                reservesByClient.add(r);
            }
        }
        LocalDateTime dataPresent = LocalDateTime.now();
        List<Reserves> reservesAct = new ArrayList<>();
        for(Reserves r: reservesByClient) {
            if(r.getStart().isAfter(dataPresent)) {
                System.out.println("ok");
                reservesAct.add(r);
            }
        }

        return reservesAct;
    }

    /**
     * Mètode que rep com a paràmetre un llistat de reserves i una variable de tipus integer
     *  que representa un estat. Es retornarà un llistat o un altre, depenent de l'estat que 
     * indiquem com a paràmetre: si es 0, retorna List de reserves pendents; si es 1 retorna
     * un llistat de reserves acceptades; si es 2 retorna un llistat de reserves declinades i
     * si es 3 retorna un llistat de reserves eliminades.
     * @param reservesClient Llistat tipus Reserves
     * @param estat Estat de la reserva
     * @return List de Reserves
     */
    public List<Reserves> getReservesByEstat(List<Reserves> reservesClient, int estat) {
        List<Reserves> reservesAcceptades = new ArrayList<>();
        List<Reserves> reservesEliminades = new ArrayList<>();
        List<Reserves> reservesDeclinades = new ArrayList<>();
        List<Reserves> reservesEnEspera = new ArrayList<>();
        for(Reserves r: reservesClient) {
            if(estat == 0 && r.getEstat() == estat) {
                reservesEnEspera.add(r);
            } else if(estat == 1 && r.getEstat() == estat) {
                reservesAcceptades.add(r);
            } else if(estat == 2 && r.getEstat() == estat) {
                reservesDeclinades.add(r);
            } else {
                reservesEliminades.add(r);
            }
        }
        if(estat == 0) {
            return reservesEnEspera;
        } else if(estat == 1) {
            return reservesAcceptades;
        } else if(estat == 2) {
            return reservesDeclinades;
        } else if(estat == 3) {
            return reservesEliminades;
        }

        return null;
        
    }

    /**
     * Mètode que rep com a paràmetre un identificador d'usuari client. Primer recupera
     * totes les reserves del client, independentment de si són anteriors a la data actual o 
     * posteriors. Després filtra el llistat per obtenir només les reserves que siguin anteriors
     * a la data actual i que a més estiguin en estat 1 (acceptades), per tant, ja gaudides pel client
     * @param idClient Identificar per poder saber les reserves antigues d'un client
     * @return Retorna un llistat de totes les recerves a partir un id del client "idClient"
     */
    public List<Reserves> getReservesAntiguesByClient(int idClient) {
        List<Reserves> allReserves = (List<Reserves>)crudRepositoriReserves.findAll();
        List<Reserves> allReservesClient = new ArrayList<>();
        for(Reserves r: allReserves) {
            if(r.getIdUsuari() == idClient) {
                allReservesClient.add(r);
            }
        }
        LocalDateTime dataPresent = LocalDateTime.now();
        List<Reserves> reservesAntigues = new ArrayList<>();
        for(Reserves r: allReservesClient) {
            if(r.getStart().isBefore(dataPresent)) {
                reservesAntigues.add(r);
            }
        }
        List<Reserves> reservesAntiguesAMostrar = new ArrayList<>();
        for(Reserves r: reservesAntigues) {
            if(r.getEstat()==1) {
                reservesAntiguesAMostrar.add(r);
            }
        }

        return reservesAntiguesAMostrar;
    }

}

