package beauty.service.service.repositori.Entitats;


import java.io.Serializable;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <h1>Classe que representa l’entitat {@link Serveis}<h1>
 * <p> Aquesta classe ens permet treballar amb l'entitat {@link Serveis}, a més, estableix un mapejat amb la classe  {@link Ofertes}
 * a partir de la clau forana id_servei, també té una relació amb la taula {@link Salo} fent referencia amb la clau forana {@link #idSalo}</p>
 */
@Entity
@Table(name="Serveis")
public class Serveis implements Serializable {
    private static final long serialVersionUID = 1L;

    @OneToMany(mappedBy="serveis", fetch = FetchType.EAGER ,cascade = CascadeType.ALL)
    private Set <Ofertes> ofertes;

    @Id
    @NotNull
    @Column(name = "idServei")
    @Size(max=11)
    @GeneratedValue(strategy = GenerationType.IDENTITY)      
    private int idServei;

    @NotNull
    @Column(name = "idSalo")
    private int idSalo;

    @NotNull
    @Column(name = "idTipusServei")
    private int idTipusServei;

    @NotNull
    @Column(name = "nomTipusServei")
    private String nomTipusServei;

    /**
     *  Constructor buit {@link Serveis} , que s'utilitzarà en la creació d'un objecte que serà l'instància d'una classe. Generalment
     *  duu a terme les operacions requerides per inicialitzar la classe abans que els mètodes siguin invocats
     *  o s'accedeixi als metòdes
     */
    public Serveis() {
    }



    
    /** 
     * Aquesta funció ens dona suport en la relació Un a Molts dins la relació entre l'entitat {@link Serveis} i {@link Ofertes}
     * @return Set<Ofertes> Retorna una llista de tipus {@link Ofertes} 
     */
    public Set<Ofertes> getOfertes() {
        return this.ofertes;
    }

    
    /** 
     * Setter que ens dona suport en la gestion i actualització de mapejat entre la classe {@link Serveis} i la classe {@link Ofertes}
     * @param ofertes Paràmetre per poder actulitzar una llista de tipus {@link Ofertes}
     */
    public void setOfertes(Set<Ofertes> ofertes) {
        this.ofertes = ofertes;
    }

    
    /** 
     * Mètode per poder treballar amb clau forana {@link #idSalo} de la classe {@link Salo} 
     * @return int Ens retorna la propietat {@link #idSalo} 
     */
    public int getIdSalo() {
        return idSalo;
    }


    
    /** 
     * Aquesta funció actualitzarà l'atribut {@link #idSalo} a partir d'un paràmetre donat,  que és una clau forana de la classe {@link Salo} 
     * @param idSalo Amb aquest valor de tipus enter, es farà server per canviar l'id {@link #idSalo} dins la classe {@link Serveis}
     */
    public void setIdSalo(int idSalo) {
        this.idSalo = idSalo;
    }


    
    /** 
     * Getter per obtenir  la clau forana {@link #idTipusServei} de la classe {@link TipusServeis}
     * @return int Ens retorna un enter, que serà clau forana de {@link #idTipusServei}
     */
    public int getIdTipusServei() {
        return idTipusServei;
    }


    
    /** 
     * La següent funció ens servirà de suport per canviar el valor de la clau forana {@link #idTipusServei}
     * de la classe {@link TipusServeis}
     * @param idTipusServei És el valor a actualitzar des de l'atribut {@link #idTipusServei}
     */
    public void setIdTipusServei(int idTipusServei) {
        this.idTipusServei = idTipusServei;
    }


    
    /** 
     * Aquest mètode ens servirà per obtenir la propietat {@link #nomTipusServei} de la classe {@link TipusServeis}.
     * @return String Ens retorna el valor de la propietat {@link #nomTipusServei} a partir d'un valor tipus String
     */
    public String getNomTipusServei() {
        return nomTipusServei;
    }


    
    /** 
     * Funció per poder canviar el valor de l'atribut {@link #nomTipusServei} 
     * @param nomTipusServei Amb aquest paràmetre de tipus String, podrem canviar un atribut {@link #nomTipusServei} donat
     */
    public void setNomTipusServei(String nomTipusServei) {
        this.nomTipusServei = nomTipusServei;
    }

    
    /** 
     * Mètode por obtenir el valor d'id {@link #idServei} 
     * @return int Retorna un enter, que serà el valor de l'id {@link #idServei}.
     */
    public int getIdServei() {
        return idServei;
    }


    
}
