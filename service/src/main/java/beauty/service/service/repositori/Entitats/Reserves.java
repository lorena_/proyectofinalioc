package beauty.service.service.repositori.Entitats;

import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * <h1>Classe que representa una entitat de tipus Reserves.</h1>
 * </p>Aqesta classe permet associar un Event amb un usuari client i un saló de bellesa. Al mateix temps, serveix de
 * suport a la classe Event.</p>
 */
@Entity
public class Reserves  {

    /**
     * S'estableix una relació de columnes de Molts a Un entre l'entitat Reserves i Usuari, amb una clau forana idUsuari que apunta
     * a l'id idUsuari de l'entitat Usuari.
     */
    @ManyToOne(fetch = FetchType.LAZY ,cascade = CascadeType.ALL)
    @JoinColumn(name = "idUsuari")
    private Usuari usuari;

    /**
     * S'estableix un mapa de relacions d'Un a Un entre l'entitat Reserves i Ofertes, amb una clau forana reserva que apunta
     * a l'idOfertes de l'entitat Ofertes.
     */
    @OneToOne(mappedBy = "reserva",fetch = FetchType.LAZY ,cascade = CascadeType.ALL) Ofertes ofertes;
        
    @Id
    @Column(name="idReserva")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Size(max=4)
    @NotNull
    private int idReserva;
    
    @Column(name="idEvent")
    @Size(max=4)
    @NotNull
    private int idEvent;

    
    @Column(name="idUsuariClient")
    @Size(max=4)
    @NotNull
    private int idUsuariClient;

    
    @NotNull
    @Column(name = "idSalo")
    @Size(max=9)
    private int idSalo;

    @Column(name = "idOferta")
    @Size(max=11)    
    private int idOferta;

    @NotNull
    @Column(name = "estat")
    @Size(max=4)    
    private int estat;

    @NotNull
    @Column(name = "text")
    @Size(max=120)
	String text;
	@NotNull
    @Column(name = "text2")
    @Size(max=120)
	String text2;
	
	@NotNull
    @Column(name = "start")
    @Size(max=45)
	LocalDateTime start;
	
	@NotNull
    @Column(name = "end")
    @Size(max=45)
	LocalDateTime end;

	@NotNull
    @Column(name = "color")
    @Size(max=45)
	String color;
  
    /** 
     * S'obté l'id d'un usuari
     * @return int Ens retorna l'id de l'usuari en tipus de dades primitiu enter
     */
    public int getIdUsuari() {
        return this.idUsuariClient;
    }

    
    /** 
     * Funció que actualitza l'id d'un usaria, passant-li com paràmetre un enter
     * @param idUsuari Paràmetre que ens permet modificar l'id de un usuari
     */
    public void setIdUsuari(int idUsuari) {
        this.idUsuariClient = idUsuari;
    }


    /** 
     * Funció per actualitzar un objecte tipus Ofertes, en la taula de relacions entre Reserves i ofertes.
     * @param ofertes Paràmetre de tipus Ofertes, el qual es farà server per actualitzar una oferta.
     */
    public void setOfertes(Ofertes ofertes) {
        this.ofertes = ofertes;
    }

/**
 * <p>Constructor de la clase {@link #Reserves(String, LocalDateTime, LocalDateTime)} </p>
 * @param text És el text que es mostrarà al client el el calendari de reserves.
 * @param start La data i temps local d'inici del calendari per triar una cita a l'hora de triar un servei.
 * @param end Fa la mateixa funció que el paràmetre {@link #start}, però en aquest cas, per la data fi.
 */
    public Reserves(String text,  LocalDateTime start, LocalDateTime end) {
        this.text = text;
        this.start = start;
        this.end = end;
    }

    /**
     * Un constructor buit, que s'utilitzarà en la creació d'un objecte que serà l'instància d'una classe. Generalment
     *  duu a terme les operacions requerides per inicialitzar la classe abans que els mètodes siguin invocats
     *  o s'accedeixi als metòdes
     */
    public Reserves() {
    }


    /** 
     * Mètode per obtenir el text que es mostrarà al client el el calendari de reserves.
     * @return String Ens retorna un objecte tipus String, en aquest cas serà el text informatiu 
     * que es mostrarà al client a l'hora que aquest trigui un a cita.
     */
    public String getText() {
        return this.text;
    }

    
    /** 
     * Mètode d’accés als camps o atributs de la nostra classe {@link #Reserves(String, LocalDateTime, LocalDateTime)}. 
     * Ho farem servir per establir o assignar un valor a l'atribut {@link #text}
     * @param text Aquest el farem servir per recuperar el text que es mostrarà a un client en la triada d'una reserva.
     */
    public void setText(String text) {
        this.text = text;
    }

    
    /** 
     * Aquesta funció la farem servir per obtenir el camp {@link #start}, que és la data d'inici de triada d'un servei
     * @return LocalDateTime  Ens retorna un data i hora sense una zona horària del sistema. Amb aquest objecte immutable, 
     * s'obté l'hora d'inici en una reserva d'un servei.
     */
    public LocalDateTime getStart() {
        return this.start;
    }

    
    /** 
     * Mètode d’accés als atribut {@link #start} de la nostra classe {@link #Reserves(String, LocalDateTime, LocalDateTime)} 
     * D'aquesta manera podrem actualitzar o modificar la data d'inici d'una reserva.
     * @param start
     */
    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    
    /** 
     * Funció que obté un objecte immutable date-time, la qual la férem server per determinar una data i hora de fi de triada d'un servei
     * @return LocalDateTime Objecte immutable date-time, que determinarà la data da fi en la qual un client tria una cita per 
     * un servei determinat.
     */
    public LocalDateTime getEnd() {
        return this.end;
    }

    
    /** 
     * Aquest mètode ens servira de suport per poder modificar una data d'inici passant-li per paràmetre la data i hora a modificar.
     * @param end Camp de tipus date-time que el farem servir per actualitzar una triada de cita específica.
     */
    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    
    /** 
     * Mètode el fent servir per poder obtenir un color, a 'hora de donar format
	 * una celda en el calendari de cites i que poden ser:
	 * <ul>
	 * <li>Groc
	 * <li>Verd
	 * <li>vermell
	 * <li>Blau
	 * </ul>
     * @return String  Ens retorna el nom d'un color amb un valor tipus String, que sera un color demanat.
     */
    public String getColor() {
        return this.color;
    }

    
    /** 
     * Funció que actualitza un color de la cel·la seleccionada en el calendari per treure
     * cites, d’aquesta manera poder guardar-la en la base de dades.
     * @param color Amb aquest paràmetre de tipus String determina el color de cada
	 *cel·la en el calendari de cites.
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * Mètode que actualitza l'id d'una reserva
     * @param idReserva Amb aquest paràmetre de tipus enter, serà l'identificador a canviarà a una reserva donada
     */
    public void setIdReserva(int idReserva) {
        this.idReserva = idReserva;
    }
    
    /** 
     * Aquesta funció guarda l'id d'un Event
     * @param idEvent És un paràmetre de tipus enter, que determinara que Event serà actualitzat.
     */
    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }


    
    /** 
     * Es un mètode d'una clau forana, la qual a partir d'un paràmetre donat actualitza l'id d'un salo
     *  dins la entitat Reserves
     * @param idSalo Paràmetre que indica a posició d'una clau forana a actualitzar.
     */
    public void setIdSalo(int idSalo) {
        this.idSalo = idSalo;
    }
    
    /** 
     * Aquesta funció es fa servir de suport en una relacion entre l'entitat  Reserves i Usuari,
     * retornant així un objecte de tipus Usuari. 
     * @return Usuari Ens retorna un objecte de tipus Usuari dins la relacion d'entitats 
     * amb la classe Usuari.
     */
    public Usuari getUsuari() {
        return this.usuari;
    }

    
    /** 
     * Mètode que actualitza un usuari en la relació establerta entre l'entitat  Reserves i Usuari.
     * @param usuari Aquest paràmetre de tipus Usuari, determina l'usuari a actualitzar.
     */
    public void setUsuari(Usuari usuari) {
        this.usuari = usuari;
    }



    
    /** 
     *Funció que opte l'id d'una Reserva
     * @return int Retorna l'identificar únic de classe Reserva.
     */
    public int getIdReserva() {
        return idReserva;
    }

    
    /** 
     * Aquesta funció es de reforç en la relació entre Reserves i la taula Salo, 
     * per tant en dins la taula Reserves, fa la funció de clau forana i en aquest cas
    * la fent servir per obtenir l'id d'un saló.
     * @return int Dins l'entitat Reserves, ens retorna la clau forana de l'entitat Salo
     */
    public int getIdSalo() {
        return idSalo;
    }

    
    /** 
     * Continuem amb les claus foranes, en aquest cas, la següent funció ens retorna
     * l'id de l'entitat Oferta.
     * @return int Des de l'entitat Reserva s'obté l'id de l'entitat Oferta, 
     * ja que està composta com una clau forana.
     */
    public int getIdOferta() {
        return idOferta;
    }

    
    /** 
     * Mètode que gestiona l'estat actual d'una reserva, ja que una reserva por estar en 4 diferents estats:
     * <ul>
     * <li>0, retorna List de reserves pendents.
     * <li>1, retorna un llistat de reserves acceptades.
     * <li>2, retorna un llistat de reserves declinades.
     * <li>3, retorna un llistat de reserves eliminades.
     * </ul>
     * @return int Ens retorna un enter, representant l'estat d'una reserva
     */
    public int getEstat() {
        return estat;
    }

    
    /** 
     * Funció per editat l'estat d'una reserve feta per un client, passan-li per paràmetre un enter reprentant
     * situació actual d'una reserva.
     * 
     * @param estat Paràmetre per actualitzar l'estat d'una reserva, prenent valors enters tals com:
     * <ul>
     * <li>0, reserves pendents.
     * <li>1, reserves acceptades.
     * <li>2, reserves declinades.
     * <li>3, reserves eliminades.
     * </ul>
     */
    public void setEstat(int estat) {
        this.estat = estat;
    }

    
    /**
     * Funció que recull l'id d'un Event. Bàsicament, un Event és un objecte de la llibreria del calendari de 
     * cites en la triada d'un servei.
     * @return int L'identificar d'un Event.
     */
    public int getIdEvent() {
        return this.idEvent;
    }


    
}