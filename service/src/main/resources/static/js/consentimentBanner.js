/**
*S'inicialitza jquery comprovant que el document este carregat
* 
*/
$(document).ready(function() { 

  /**
*S'afegeix el banner consent
*/
  $(document.body).append(
    '<div class="cookie-banner">' +
    '<div class="cookie-close accept-cookie"></div>' +
    '<div class="container">' +
    '<p>Utilitzem cookies pr&#242;pies per a un &#250;s t&#233;cnic i per a un bon funcionament dels nostres serveis.<br>' +
    'Si accepteu o continueu navegant, considerem que accepteu les condicions.<br>' +
    '<a href="https://www.boe.es/buscar/pdf/2018/BOE-A-2018-16673-consolidado.pdf">' +
    'Per m&#233;s infomaci&#243; sobre la protecci&#243; de dades</a></p>' +
    '<button type="button" class="btn acceptar-cookie">Aceptar</button>' +
    '</div></div>');

    var cookie = checkCookie();
    var cookieContent = $('.cookie-banner');

    if (cookie) {
      cookieContent.hide();
    }else{

      cookieContent.show();
    }

    /**
* S'agraga una cookie "consentimentDades" amb una data de caducitat de un any
*/
    $('.acceptar-cookie').click(function () {
      gravaCookieAmbDataExpirar("consentimentDades", "accepted", 365);
      cookieContent.hide(500);
    });


   /**
*Amb un valor bolea, comprova que exista la cookie "consentimentDades"
*  
 * @returns {Boolean} cookie 
 */

	function checkCookie() {
		var cookie = false;
		var check = getCookie("consentimentDades");
		if (check !== "") {
		  return cookie = true;
		} else {
			return cookie = false; 
		}
		
	  }
});